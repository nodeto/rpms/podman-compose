%define version 1.0.3

Name:           nodeto.com-podman-compose
Version:        %{version}
Release:        2%{?dist}
Summary:        Podman Compose
BuildArch:      x86_64

License:       GPL
Source:        http://nexus.home.nodeto.com:8081/repository/generic/python/podman-compose-%{version}.tar.gz

Requires:       podman
Requires:       podman-plugins
Requires:       python38
Requires:       python38-pyyaml
Requires:       python38-dotenv

BuildRequires:       python38
BuildRequires:       python38-pyyaml
BuildRequires:       python38-dotenv

%description
Podman-compose will define and launch services in pods

%global debug_package %{nil}
%prep
%setup -q -n podman-compose-%{version}

%build
true

%install
/usr/bin/python3.8 -m pip install --no-index --root "%{buildroot}" .

%files
%{_bindir}/*
/usr/lib/python3.8/site-packages/*
